const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const exphbs = require('express-handlebars');
const todoRouter = require('./routes/todos');
const authRouter = require('./routes/auth');
const PORT = process.env.PORT || 3000;

const app = express();
app.use(express.json());


const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
});



app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

app.use(express.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'public')));

app.use(todoRouter);
app.use("/auth", authRouter);


async function start() {
    try {
        await mongoose.connect('mongodb+srv://dergunov:dergunov.aa@cluster0.ko4mj.mongodb.net/myDatabase', {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        })
        app.listen(PORT, () => {
            console.log(`Server started on port ${PORT}`);
        });
    }catch (e) {
        console.log(e);
    }
}
start();


