const { Schema, model } = require('mongoose');

const schema = new Schema({
    productName: {
        type: String,
        required: true
    },
    price: {
      type: String,
      required: true
    },
    // imgSrc: {
    //     type: String,
    //     required: true
    // }
});


module.exports = model('Todo', schema);