const {Schema, model} = require('mongoose')


const Delivery = new Schema({
    clientName: {
        type: String,
        required: true
    },
    clientSurname: {
        type: String,
        required: true
    },
    clientPhoneNumber: {
        type: Number,
        required: true
    },
    clientEmail: {
        type: String
    },
    deliveryAddress: {
        type: String,
        required: true
    }
})

module.exports = model('Delivery', Delivery);