const storageRoleSpan = document.getElementById('storageUserRole');
const storageNameSpan = document.getElementById('storageUserName');
const cookiesArray = document.cookie;
const splitedArrayCookies = cookiesArray.split(";");
const userRole = splitedArrayCookies[0].split('=');
const userNAME = splitedArrayCookies[1].split('=');
storageNameSpan.innerHTML = `User name: ${userNAME[1]}`;
storageRoleSpan.innerHTML = `User role: ${userRole[1]}`;
