const {Router} = require('express');
const Delivery = require('../models/Delivery');
const User = require('../models/User');
const Todo = require('../models/Todo');
const router = Router();
const controller = require('../controller/authController');
const {check} = require('express-validator');

router.get('/', async (req, res) => {
    const todos = await Todo.find({}).lean()

    res.render('index', {
        title: 'Main page',
        todos
    });
});

router.get('/auth', (req, res) => {
    res.render('auth', {
        title: 'authPage'
    });
})

router.post('/auth',
    [
        check('username', "Имя пользователя не может быть пустым").notEmpty(),
        check('email', "Введите корректный email").isEmail(),
        check('password', "Пароль должен быть от 4 до 10 символов").isLength({min: 4, max: 10})
],
    controller.registration);


// router.post('/login',
//     [
//         check('username', "Имя пользователя не может быть пустым").notEmpty(),
//         check('email', "Введите корректный email").isEmail(),
//         check('password', "Пароль должен быть от 4 до 10 символов").isLength({min: 4, max: 10})
//     ],
//     controller.registration);

router.post('/login', controller.login);


router.get('/create', (req, res) => {
        res.render('create', {
            title: 'Create page'
        });
})

router.get('/delivery', (req,res) => {
    res.render('delivery', {
        title: 'Delivery page'
    });
})

router.get('/products', (req, res) => {
    res.render('products', {
        title: 'Product page'
    });
})

router.get('/InfoAPIPage', (req, res) => {
    res.render('InfoAPIPage', {
        title: 'Info API page'
    });
})

router.get('/login', (req, res) => {
    res.render('login', {
        title: 'Login page'
    });
})


router.get('/cart', (req, res) => {
    res.render('cart', {
        title: 'Your cart'
    });
})


router.get('/userPage', (req, res) => {
    try {


        res.render('userPage', {
            title: 'Your account'
        });
    } catch (e) {

    }
})

// router.post('/userPage', async (req,res) => {
//     try {
//         const {username, email, password} = req.body;
//         const currentUser = await User.findOne({username})
//
//     } catch (e) {
//
//     }
// })


    router.post('/create', async (req, res) => {
        try {
                const todo = new Todo({
                    productName: req.body.productName,
                    price: req.body.price,
                    // imgSrc: req.body.imgSrc.value
                })
                await todo.save()
                res.redirect('/')
        } catch (e) {
            res.status(400).json({message: 'Creating product error'})
        }
    })


router.post('/delivery', async (req, res) => {
    try {
        const delivery = new Delivery({
            clientName: req.body.fullname,
            clientSurname: req.body.surname,
            clientPhoneNumber: req.body.phoneNum,
            clientEmail: req.body.email,
            deliveryAddress: req.body.delAddress,
        })
        await delivery.save();
        // res.render('userPage');
        res.send('Successfully');
    } catch (e) {
        res.status(400).json({message: 'Invalid data to deliver'})
    }
})





module.exports = router;