const app = require('express');
const User = require('../models/User');
const Role = require('../models/Role');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const {secret} = require("../config");
const cookieParser = require('cookie-parser');


const generateAccessToken = (id, roles) => {
    const payload = {
        id,
        roles
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}


class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(400).json({message: "Ошибка при регистрации", errors})
            }
            const {username, email, password} = req.body;
            const candidate = await User.findOne({username});
            if (candidate) {
                return res.status(400).json({message: "Пользователь уже существует, войдите в аккаунт"});
            }
            const hashedPassword = bcrypt.hashSync(password, 7);
            const userRole = await Role.findOne({value: "USER"});
            const user = new User({username, email, password: hashedPassword, roles: userRole.value});
            await user.save();
            return res.json({message: "Пользователь успешно зарегистрирован"});
        }    catch (e) {
            console.log(e);
            res.status(400).json({message: 'Registration error'});
        }
    }


    async login(req, res) {
        try {
            const {username, password} = req.body;
            const user = await User.findOne({username});
            if(!user) {
                return res.status(400).json({message: `Пользователь с именем ${username} не найден`});
            }
            const validPassword = bcrypt.compareSync(password, user.password)
            if(!validPassword) {
                return res.status(400).json({message: `Введен неверный пароль`});
            }
            const token = generateAccessToken(user._id, user.roles);
            // return res.json({token});
            res.cookie('NAME', username);
            res.cookie('ROLES', user.roles);
            return res.render('userPage');
        } catch (e) {
            console.log(e);
            res.status(400).json({message: 'Login error'});
        }
    }




    async createProd(req, res) {
        try {
            const userRole = await Role.findOne('ADMIN');
            alert(`Welcome ${userRole}, you can start to use service`);
            res.render('create')
        } catch (e) {
            res.status(400).json({message: 'Your role is invalid'});
        }
    }

    async getUsers(req, res) {
        try {
            const users = await User.find();
            // res.render('userPage');
            res.json(users);
        } catch (e) {
            console.log(e)
        }
    }
}


module.exports = new authController();
